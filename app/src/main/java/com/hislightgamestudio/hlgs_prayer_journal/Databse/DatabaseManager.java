/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
      http://www.apache.org/licenses/LICENSE-2.0                              *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: DatabaseManager.java                                              *
 Author: john  on  5/31/2016.                                                 *
 Created On: 5/31/2016 - 9:5                                                  *
 Last Updated On: 5/25/16 9:43 PM                                             *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal.Databse;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.hislightgamestudio.hlgs_prayer_journal.Model.JournalModel;

import java.util.ArrayList;

/**
 * Created by john on 5/14/16.
 */
public class DatabaseManager{
    private SQLiteDatabase db;
    private static final String DB_NAME = "entry";

    Context context;

    private static final int DB_VERSION = 1;
    private static final String TABLE_NAME= "entry_table";
    private static final String TABLE_ROW_ID = "_id";
    private static final String TABLE_ROW_GRATEFUL = "entry_grateful";
    private static final String TABLE_ROW_ANSWER = "entry_answer";
    private static final String TABLE_ROW_PERSONAL = "entry_personal";
    private static final String TABLE_ROW_OTHERS = "entry_others";
    private static final String TABLE_ROW_DATE = "entry_date";

    public DatabaseManager(Context context){
        this.context = context;
        CustomSQLiteOpenHelper helper = new CustomSQLiteOpenHelper(context);
        this.db = helper.getWritableDatabase();
    }

    public class CustomSQLiteOpenHelper extends SQLiteOpenHelper {

        public CustomSQLiteOpenHelper(Context context){
            super(context, DB_NAME, null, DB_VERSION);
        }

        //Override Methods
        @Override
        public void onCreate(SQLiteDatabase db) {
            String newTableQueryString = "create table "
                    + TABLE_NAME + " ("
                    + TABLE_ROW_ID
                    + " integer primary key autoincrement not null,"
                    + TABLE_ROW_GRATEFUL
                    + " text not null,"
                    + TABLE_ROW_ANSWER
                    + " text not null,"
                    + TABLE_ROW_PERSONAL
                    + " text not null,"
                    + TABLE_ROW_OTHERS
                    + " text not null,"
                    + TABLE_ROW_DATE
                    + " text not null" + ");";
            db.execSQL(newTableQueryString);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
            db.execSQL(DROP_TABLE);
            onCreate(db);
        }
    }

    public void addRow(JournalModel journalObj){
        ContentValues values = prepareData(journalObj);
        try{
            db.insert(TABLE_NAME, null, values);
        }catch (Exception e){
            Log.e("DB ERROR", e.toString());
            e.printStackTrace();
        }
    }

    private ContentValues prepareData(JournalModel journalObj){
        ContentValues values = new ContentValues();
        values.put(TABLE_ROW_GRATEFUL, journalObj.getGrateful());
        values.put(TABLE_ROW_ANSWER, journalObj.getAnswers());
        values.put(TABLE_ROW_PERSONAL, journalObj.getPersonal());
        values.put(TABLE_ROW_OTHERS, journalObj.getOthers());
        values.put(TABLE_ROW_DATE, journalObj.getDate());
        return values;
    }

    public JournalModel getRowAsObject(int rowID){
        JournalModel rowJournalObj = new JournalModel();
        Cursor cursor;
        try{
            cursor = db.query(TABLE_NAME, new String[]{
                    TABLE_ROW_ID, TABLE_ROW_GRATEFUL, TABLE_ROW_ANSWER, TABLE_ROW_PERSONAL, TABLE_ROW_OTHERS, TABLE_ROW_DATE},
                    TABLE_ROW_ID + "=" + rowID, null,
                    null, null, null, null);
            cursor.moveToFirst();
            if(!cursor.isAfterLast()){
                prepareSendObject(rowJournalObj, cursor);
            }
        }catch(SQLException e){
            Log.e("DB ERROR", e.toString());
            e.printStackTrace();
        }
        return rowJournalObj;
    }

    private void prepareSendObject(JournalModel rowObj, Cursor cursor){
        rowObj.setId(cursor.getInt(cursor.getColumnIndexOrThrow(TABLE_ROW_ID)));
        rowObj.setGrateful(cursor.getString(cursor.getColumnIndexOrThrow(TABLE_ROW_GRATEFUL)));
        rowObj.setAnswers(cursor.getString(cursor.getColumnIndexOrThrow(TABLE_ROW_ANSWER)));
        rowObj.setPersonal(cursor.getString(cursor.getColumnIndexOrThrow(TABLE_ROW_PERSONAL)));
        rowObj.setOthers(cursor.getString(cursor.getColumnIndexOrThrow(TABLE_ROW_OTHERS)));
        rowObj.setDate(cursor.getString(cursor.getColumnIndexOrThrow(TABLE_ROW_DATE)));
    }

    public void deleteRow(int rowID){
        try{
            db.delete(TABLE_NAME, TABLE_ROW_ID
            + "=" + rowID, null);
        }catch(Exception e){
            Log.e("DB ERROR", e.toString());
            e.printStackTrace();
        }
    }

    public void updateRow(int rowID, JournalModel journalObj){
        ContentValues values = prepareData(journalObj);

        String whereClause = TABLE_ROW_ID + "=?";
        String whereArgs[] = new String[] {String.valueOf(rowID)};

        db.update(TABLE_NAME, values, whereClause, whereArgs);
    }

    public ArrayList<JournalModel> getAllData(){
        ArrayList<JournalModel> allRowsObj = new ArrayList<JournalModel>();
        Cursor cursor;
        JournalModel rowContactObj;

        String[] columns = new String[] { TABLE_ROW_ID, TABLE_ROW_GRATEFUL,
                TABLE_ROW_ANSWER, TABLE_ROW_PERSONAL, TABLE_ROW_OTHERS, TABLE_ROW_DATE };

        try {

            cursor = db
                    .query(TABLE_NAME, columns, null, null, null, null, null);
            cursor.moveToFirst();

            if (!cursor.isAfterLast()) {
                do {
                    rowContactObj = new JournalModel();
                    rowContactObj.setId(cursor.getInt(0));
                    prepareSendObject(rowContactObj, cursor);
                    allRowsObj.add(rowContactObj);

                } while (cursor.moveToNext()); // try to move the cursor's
                // pointer forward one position.
            }
        } catch (SQLException e) {
            Log.e("DB ERROR", e.toString());
            e.printStackTrace();
        }

        return allRowsObj;
    }
}

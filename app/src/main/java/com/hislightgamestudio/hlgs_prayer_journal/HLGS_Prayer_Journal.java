/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
      http://www.apache.org/licenses/LICENSE-2.0                              *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: HLGS_Prayer_Journal.java                                          *
 Author: john  on  5/31/2016.                                                 *
 Created On: 5/26/2016 - 9:3                                                  *
 Last Updated On: 5/26/16 8:40 PM                                             *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.hislightgamestudio.hlgs_prayer_journal.Views.AddNewJournalEntry;
import com.hislightgamestudio.hlgs_prayer_journal.Views.JournalDesFragment;
import com.hislightgamestudio.hlgs_prayer_journal.Views.JournalEntriesFragment;
import com.hislightgamestudio.hlgs_prayer_journal.Views.JournalTutFragment;

public class HLGS_Prayer_Journal extends AppCompatActivity
        implements OnSelectedEntryChangeListener{

    final static String TAG = "MainActivity";
    public final static int ENTRY_ADD_REQ_CODE = 100;
    public final static int ENTRY_UPDATE_REQ_CODE = 101;
    public final static String REQ_TYPE = "req_type";
    public final static String ITEM_POSITION = "item_position";

    private boolean mIsDynamic;

    FragmentManager fm;
    JournalEntriesFragment jef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hlgs__prayer__journal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fm = getSupportFragmentManager();
        Fragment jdf = fm.findFragmentById(R.id.fragmentDescription);

        mIsDynamic = jdf == null || !jdf.isInLayout();

        if(mIsDynamic){
            //Begin transaction
            FragmentTransaction ft = fm.beginTransaction();

            //Create and add the fragment
            jef = new JournalEntriesFragment();
            ft.add(R.id.layoutRoot, jef, "entryList");

            //commit the changes
            ft.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_hlgs__prayer__journal, menu);
        return true;
    }

    @Override
    public void onBackPressed(){


        if(!mIsDynamic){
            if(getSupportFragmentManager().findFragmentById(R.id.fragmentEntries).isVisible()){
                finish();
            }

            FragmentTransaction ft = fm.beginTransaction();

            getSupportFragmentManager().popBackStackImmediate();
            ft.addToBackStack(null);
            ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
            ft.commit();
        } else {
            if(fm.getBackStackEntryCount() >= 1){
                getSupportFragmentManager().popBackStackImmediate();
            } else {
                finish();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        switch (item.getItemId()){
            case R.id.action_add:
                if(mIsDynamic) {
                    AddNewJournalEntry anje;
                    FragmentTransaction ft = fm.beginTransaction();

                    anje = new AddNewJournalEntry();

                    //replace current view with FAQ
                    ft.replace(R.id.layoutRoot, anje, "AddNew");
                    //Add to back stack
                    ft.addToBackStack(null);
                    //set an animation
                    ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

                    //commit transactions
                    ft.commit();
                }else{
                    AddNewJournalEntry anje;
                    FragmentTransaction ft = fm.beginTransaction();

                    anje = new AddNewJournalEntry();

                    //replace current fragments
                    ft.hide(getSupportFragmentManager().findFragmentById(R.id.fragmentDescription));
                    ft.hide(getSupportFragmentManager().findFragmentById(R.id.fragmentEntries));
                    ft.add(R.id.layoutWideRoot, anje, "AddNew");
                    //Add to back stack
                    ft.addToBackStack(null);
                    //set an animation
                    ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

                    //commit transactions
                    ft.commit();
                }

                break;

            case R.id.action_FAQ:
                if(mIsDynamic) {
                    JournalTutFragment jtf;
                    FragmentTransaction ft = fm.beginTransaction();

                    jtf = new JournalTutFragment();

                    //replace current view with FAQ
                    ft.replace(R.id.layoutRoot, jtf, "FAQ");
                    //Add to back stack
                    ft.addToBackStack(null);
                    //set an animation
                    ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

                    //commit transactions
                    ft.commit();
                }else{
                    JournalTutFragment jtf;
                    FragmentTransaction ft = fm.beginTransaction();

                    jtf = new JournalTutFragment();


                    //replace current fragments
                    ft.hide(getSupportFragmentManager().findFragmentById(R.id.fragmentDescription));
                    ft.hide(getSupportFragmentManager().findFragmentById(R.id.fragmentEntries));
                    ft.add(R.id.layoutWideRoot, jtf, "FAQ");
                    //Add to back stack
                    ft.addToBackStack(null);
                    //set an animation
                    ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

                    //commit transactions
                    ft.commit();
                }
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ENTRY_ADD_REQ_CODE) {
            if (resultCode == RESULT_OK) {
                // Notify the adapter that underlying data set has changed
                jef.getCAdapter().notifyDataSetChanged();
            } else if (resultCode == RESULT_CANCELED) {

            }
        }
    }

    @Override
    public void onSelectedEntryChangeListener(int entryID) {
        JournalDesFragment jdf;
        jdf = (JournalDesFragment) fm.findFragmentById(R.id.fragmentDescription);

        if(mIsDynamic){
            FragmentTransaction ft = fm.beginTransaction();

            jdf = new JournalDesFragment();

            Bundle args = new Bundle();
            args.putInt(jdf.ENTRY_ID, entryID);
            jdf.setArguments(args);

            //replace the list with the description
            ft.replace(R.id.layoutRoot, jdf, "entryDescription");
            //Add to back stack
            ft.addToBackStack(null);
            //set an animation
            ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);

            //commit transactions
            ft.commit();
        }else if(jdf != null){
            jdf.setEntry(entryID);
        }
    }
}

/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
      http://www.apache.org/licenses/LICENSE-2.0                              *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: JournalDescActivity.java                                          *
 Author: john  on  6/11/2016.                                                 *
 Created On: 6/11/2016 - 10:55                                                *
 Last Updated On: 6/11/16 10:55 PM                                            *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.hislightgamestudio.hlgs_prayer_journal.Views.JournalDesFragment;

public class JournalDescActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hlgs__prayer__journal_desc);

        Intent intent = getIntent();
        int entryID = intent.getIntExtra("entryID", -1);

        if(entryID != -1){
            FragmentManager fm = getSupportFragmentManager();
            JournalDesFragment jdf = (JournalDesFragment) fm.findFragmentById(R.id.fragmentDescription);
            jdf.setEntry(entryID);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_hlgs__prayer__journal, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}

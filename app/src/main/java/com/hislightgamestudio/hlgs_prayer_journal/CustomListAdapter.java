/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
 http://www.apache.org/licenses/LICENSE-2.0                                   *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: CustomListAdapter.java                                            *
 Author: john  on  6/11/2016.                                                 *
 Created On: 6/11/2016 - 4:14                                                 *
 Last Updated On: 6/11/16 4:14 PM                                             *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal;

import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hislightgamestudio.hlgs_prayer_journal.Databse.DatabaseManager;
import com.hislightgamestudio.hlgs_prayer_journal.Model.JournalModel;

import java.util.ArrayList;

public class CustomListAdapter extends BaseAdapter{
    DatabaseManager dm;
    ArrayList<JournalModel> journalModels;
    LayoutInflater inflater;
    Context _context;

    public CustomListAdapter(Context context){
        journalModels = new ArrayList<>();
        _context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dm = new DatabaseManager(_context);
        journalModels = dm.getAllData();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        //refetching the new data from database
        journalModels = dm.getAllData();
    }

    public void delRow(int delPosition){
        dm.deleteRow(journalModels.get(delPosition).getId());
        journalModels.remove(delPosition);
    }

    @Override
    public int getCount() {
        return journalModels.size();
    }

    @Override
    public Object getItem(int position) {
        return journalModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.journal_list_desc, null);
            viewHolder = new ViewHolder();

            viewHolder.entry_grateful = (TextView)convertView.findViewById(R.id.entry_grateful_desc);
            viewHolder.entry_answer = (TextView)convertView.findViewById(R.id.entry_answer_desc);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        JournalModel journalObj = journalModels.get(position);
        Resources res = convertView.getResources();

        if(journalModels.isEmpty()){
            viewHolder.entry_grateful.setText("");
            viewHolder.entry_answer.setText("");
        } else {
            CharSequence gratefulText = Html.fromHtml(String.format(res.getString(R.string.gratefulText), journalObj.getGrateful()));
            CharSequence answersText = Html.fromHtml(String.format(res.getString(R.string.answersText), journalObj.getAnswers()));

            viewHolder.entry_grateful.setText(gratefulText);
            viewHolder.entry_answer.setText(answersText);
        }

        return convertView;
    }

    class ViewHolder{
        TextView entry_grateful, entry_answer;
    }
}
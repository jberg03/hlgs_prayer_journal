/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
      http://www.apache.org/licenses/LICENSE-2.0                              *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: AddNewJournalEntry.java                                           *
 Author: john  on  6/4/2016.                                                  *
 Created On: 6/4/2016 - 7:18                                                  *
 Last Updated On: 6/4/16 7:18 PM                                              *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal.Views;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hislightgamestudio.hlgs_prayer_journal.Databse.DatabaseManager;
import com.hislightgamestudio.hlgs_prayer_journal.HLGS_Prayer_Journal;
import com.hislightgamestudio.hlgs_prayer_journal.Model.JournalModel;
import com.hislightgamestudio.hlgs_prayer_journal.R;

import java.util.Calendar;

public class AddNewJournalEntry extends Fragment implements View.OnClickListener{



    private TextView grateful, answer, personal, other, date;
    private Button doneButton;

    private int reqType;
    private int rowID;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View viewHierarchy = inflater.inflate(R.layout.journal_add_entry, container, false);

        reqType = HLGS_Prayer_Journal.ENTRY_ADD_REQ_CODE;

        bindViews(viewHierarchy);

        setListeners();

        if (reqType == HLGS_Prayer_Journal.ENTRY_UPDATE_REQ_CODE) {

            rowID = 1;
            initialize(rowID);
        }
        return viewHierarchy;
    }

    private void initialize(int position){
        DatabaseManager dm = new DatabaseManager(this.getContext());
        JournalModel journalObj = dm.getRowAsObject(position);

        grateful.setText(journalObj.getGrateful());
        answer.setText(journalObj.getAnswers());
        personal.setText(journalObj.getPersonal());
        other.setText(journalObj.getOthers());
        date.setText(journalObj.getDate());
    }

    private void bindViews(View view) {
        grateful = (TextView) view.findViewById(R.id.grateful_for);
        answer = (TextView) view.findViewById(R.id.answered_prayers);
        personal = (TextView) view.findViewById(R.id.personal_prayers);
        other = (TextView) view.findViewById(R.id.others_prayers);
        date = (TextView) view.findViewById(R.id.date_text);
        doneButton = (Button) view.findViewById(R.id.doneBtn);

        Calendar c = Calendar.getInstance();
        int cDay = c.get(Calendar.DAY_OF_MONTH);
        int cMonth = c.get(Calendar.MONTH) + 1;
        int cYear = c.get(Calendar.YEAR);

        String formattedDate = Integer.toString(cMonth) + "/" + Integer.toString(cDay) + "/" + Integer.toString(cYear);

        Resources res = getResources();
        String text = String.format(res.getString(R.string.date), formattedDate);

        date.setText(text);
    }

    private void setListeners(){
        doneButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.doneBtn){
            prepareSendData();
        }
    }

    private void prepareSendData() {

        if (TextUtils.isEmpty(grateful.getText().toString())
                && TextUtils.isEmpty(answer.getText().toString())
                && TextUtils.isEmpty(personal.getText().toString())
                && TextUtils.isEmpty(other.getText().toString())) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                    AddNewJournalEntry.this.getContext());
            alertDialogBuilder.setTitle("Empty fields");
            alertDialogBuilder.setMessage("Please fill at least one of the fields")
                    .setCancelable(true);
            alertDialogBuilder.setNegativeButton("OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });
            alertDialogBuilder.show();
        } else {
            JournalModel journal = new JournalModel();

            Calendar c = Calendar.getInstance();
            int cDay = c.get(Calendar.DAY_OF_MONTH);
            int cMonth = c.get(Calendar.MONTH) + 1;
            int cYear = c.get(Calendar.YEAR);

            String formattedDate = Integer.toString(cMonth) + "/" + Integer.toString(cDay) + "/" + Integer.toString(cYear);

            journal.setGrateful(grateful.getText().toString());
            journal.setAnswers(answer.getText().toString());
            journal.setPersonal(personal.getText().toString());
            journal.setOthers(other.getText().toString());
            journal.setDate(formattedDate);

            DatabaseManager dm = new DatabaseManager(this.getContext());

            //Update if update code is sent, otherwise just a new row
            if (reqType == HLGS_Prayer_Journal.ENTRY_UPDATE_REQ_CODE) {
                dm.updateRow(rowID, journal);
            } else {
                dm.addRow(journal);
            }

            //Go back to previous fragment
            getFragmentManager().popBackStackImmediate();
        }
    }
}

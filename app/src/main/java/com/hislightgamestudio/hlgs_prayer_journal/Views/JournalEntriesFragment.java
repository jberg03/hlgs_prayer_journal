/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
      http://www.apache.org/licenses/LICENSE-2.0                              *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: AddEntryFragment.java                                             *
 Author: john  on  5/31/2016.                                                 *
 Created On: 5/31/2016 - 9:2                                                  *
 Last Updated On: 5/26/16 9:42 PM                                             *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal.Views;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.hislightgamestudio.hlgs_prayer_journal.CustomListAdapter;
import com.hislightgamestudio.hlgs_prayer_journal.OnSelectedEntryChangeListener;
import com.hislightgamestudio.hlgs_prayer_journal.R;

public class JournalEntriesFragment extends ListFragment {

    private ListView listentries;
    private CustomListAdapter cAdapter;
    OnSelectedEntryChangeListener mCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View viewHierarchy = inflater.inflate(R.layout.journal_entries, container, false);

        cAdapter = new CustomListAdapter(getContext());
        setListAdapter(cAdapter);

        return viewHierarchy;
    }

    public CustomListAdapter getCAdapter(){
        return this.cAdapter;
    }

    public ListView getList(){
        return listentries;
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        try{
            mCallback = (OnSelectedEntryChangeListener) context;
        }catch (ClassCastException e){
            throw new ClassCastException(context.toString()
                + " must implement OnSelectedEntryChangeListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        // Send the event to the host activity
        mCallback.onSelectedEntryChangeListener(position);

        getListView().setItemChecked(position, true);
    }

}
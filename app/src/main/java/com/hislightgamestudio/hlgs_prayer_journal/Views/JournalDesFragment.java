/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
      http://www.apache.org/licenses/LICENSE-2.0                              *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: JournalDesFragment.java                                           *
 Author: john  on  6/2/2016.                                                  *
 Created On: 6/2/2016 - 8:29                                                  *
 Last Updated On: 6/2/16 8:29 PM                                              *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal.Views;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hislightgamestudio.hlgs_prayer_journal.Databse.DatabaseManager;
import com.hislightgamestudio.hlgs_prayer_journal.Model.JournalModel;
import com.hislightgamestudio.hlgs_prayer_journal.R;

import java.util.ArrayList;

public class JournalDesFragment extends Fragment {
    public static final String ENTRY_ID = "entry id";
    public static final int ENTRY_ID_NOT_SET = -1;

    private int currentEntry = -1;

    TextView grateful, answers, personal, others, date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(savedInstanceState != null){
            currentEntry = savedInstanceState.getInt(ENTRY_ID);
        }

        View viewHierarchy = inflater.inflate(R.layout.journal_description, container, false);

        grateful = (TextView) viewHierarchy.findViewById(R.id.entry_grateful);
        answers = (TextView) viewHierarchy.findViewById(R.id.entry_answer);
        personal = (TextView) viewHierarchy.findViewById(R.id.entry_personal);
        others = (TextView) viewHierarchy.findViewById(R.id.entry_other);
        date = (TextView) viewHierarchy.findViewById(R.id.entry_date);

        return viewHierarchy;
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle args = getArguments();
        if(args != null){
            setEntry(args.getInt(ENTRY_ID));
        }else if (currentEntry != ENTRY_ID_NOT_SET) {
            setEntry(currentEntry);
        }else{
            setEntry(0);
        }
    }

    public void setEntry(int entryID){
        DatabaseManager dm = new DatabaseManager(this.getContext());
        ArrayList<JournalModel> journalModels;

        journalModels = dm.getAllData();
        Resources res = getResources();

        if(journalModels.isEmpty()){
            grateful.setText("");
            answers.setText("");
            personal.setText("");
            others.setText("");
            date.setText("");
        } else {
            //Use the HTML class to use string formatting
            CharSequence gratefulText = Html.fromHtml(String.format(res.getString(R.string.gratefulText), journalModels.get(entryID).getGrateful()));
            CharSequence answersText = Html.fromHtml(String.format(res.getString(R.string.answersText), journalModels.get(entryID).getAnswers()));
            CharSequence personalText = Html.fromHtml(String.format(res.getString(R.string.personalText), journalModels.get(entryID).getPersonal()));
            CharSequence othersText = Html.fromHtml(String.format(res.getString(R.string.othersText), journalModels.get(entryID).getOthers()));
            CharSequence dateText = Html.fromHtml(String.format(res.getString(R.string.dateText), journalModels.get(entryID).getDate()));

            grateful.setText(gratefulText);
            answers.setText(answersText);
            personal.setText(personalText);
            others.setText(othersText);
            date.setText(dateText);

            currentEntry = entryID;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // Save the current article selection in case we need to recreate the fragment
        outState.putInt(ENTRY_ID, currentEntry);
    }
}

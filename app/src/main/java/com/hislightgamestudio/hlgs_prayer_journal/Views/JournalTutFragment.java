/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
      http://www.apache.org/licenses/LICENSE-2.0                              *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: JournalTutFragment.java                                           *
 Author: john  on  7/4/2016.                                                  *
 Created On: 7/4/2016 - 4:23                                                  *
 Last Updated On: 7/4/16 4:23 PM                                              *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal.Views;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.hislightgamestudio.hlgs_prayer_journal.R;

public class JournalTutFragment extends Fragment {

    public JournalTutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View viewHierarchy = inflater.inflate(R.layout.journal_tutorial, container, false);

        WebView webView = (WebView) viewHierarchy.findViewById(R.id.tutorial_view);
        webView.loadUrl("file:///android_asset/tutorial.html");

        return viewHierarchy;
    }

}

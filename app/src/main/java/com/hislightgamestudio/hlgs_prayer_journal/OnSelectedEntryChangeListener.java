/******************************************************************************
 ******************************************************************************
 License                                                                      *
 ******************************************************************************
 Copyright (c) 2016.  His Light Game Studio                                   *
                                                                              *
 Licensed under the Apache License, Version 2.0 (the "License");              *
 you may not use this file except in compliance with the License.             *
 You may obtain a copy of the License at                                      *
                                                                              *
      http://www.apache.org/licenses/LICENSE-2.0                              *
                                                                              *
 Unless required by applicable law or agreed to in writing, software          *
 distributed under the License is distributed on an "AS IS" BASIS,            *
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.     *
 See the License for the specific language governing permissions and          *
 limitations under the License.                                               *
                                                                              *
 ******************************************************************************
 File Info                                                                    *
 ******************************************************************************
 Project: HLGS_Prayer_Journal                                                 *
 File Name: OnSelectedEntryChangeListener.java                                *
 Author: john  on  6/4/2016.                                                  *
 Created On: 6/4/2016 - 6:25                                                  *
 Last Updated On: 6/4/16 6:25 PM                                              *
 ******************************************************************************/

package com.hislightgamestudio.hlgs_prayer_journal;

public interface OnSelectedEntryChangeListener {
    void onSelectedEntryChangeListener(int entryID);
}
